class Consul < FPM::Cookery::Recipe
	name 'minio'
	version '1.0'
	revision '1'

	description 'Minio is a high performance distributed object storage server, designed for large-scale private cloud infrastructure.'
	license 'Apache 2.0'
	vendor 'Minio'
	homepage 'http://minio.io'

	source "https://dl.minio.io/server/#{name}/release/linux-#{lookup('arch')}/#{name}"
	sha256 'a7ec3b789a8e7a28ef59741a77fa9fea4287ad6b62ad37d4f86ac9789f11f2dc'

	post_install 'post.sh'
	post_uninstall 'post.sh'

	def build
	end

	def install
		lib('systemd/system').install workdir('systemd.service'), "#{name}.service"
		bin.install name
	end
end
