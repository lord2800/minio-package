#!/bin/bash

set -e

USER=minio
GROUP=$USER
DATA_DIR=/var/lib/$USER
SERVICE=${USER}.service

systemctl daemon-reload

case "$1" in
	configure)
		if ! getent group $GROUP > /dev/null 2>&1; then
			addgroup --system $GROUP --quiet
		fi

		if ! getent passwd $GROUP > /dev/null 2>&1; then
			adduser --system --home $DATA_DIR --no-create-home --ingroup $GROUP --disabled-password --shell /sbin/nologin $USER
		else
			/usr/bin/chsh -s /sbin/nologin $USER
		fi

		mkdir -p $DATA_DIR chown -R $USER:$GROUP $DATA_DIR
		systemctl enable $SERVICE 2>&1 >/dev/null
		systemctl start $SERVICE 2>&1 > /dev/null
	;;
	remove)
		rmdir --ignore-fail-on-non-empty $DATA_DIR
		systemctl stop $SERVICE 2>&1 > /dev/null
	;;
    purge)
    	rm -rf $DATA_DIR
    	systemctl stop $SERVICE 2>&1 > /dev/null
    	systemctl disable $SERVICE 2>&1 >/dev/null || true
    	deluser $USER || true
    	delgroup $GROUP || true
	;;
esac
